﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using KingOfTokyo.Network.Messages;
using KingOfTokyo.Game;
using System.Windows.Threading;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using KingOfTokyo.Dialogues;

namespace KingOfTokyo {
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {

        public Home home;

        public MainWindow() {
            InitializeComponent();

            home = new Home(this);
            Main.Content = home;
        }

        public void Affiche(string text, Label label, Button button = null) {
            label.Content = text;
        }
        public void Affiche(string text, Button label) {
            label.Content = text;
        }



        private void Window_Closed(object sender, EventArgs e) {
            Environment.Exit(0);
        }
    }
}
