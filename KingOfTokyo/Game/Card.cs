﻿using System;

namespace KingOfTokyo.Game {
    [Serializable]
    public abstract class Card {

        public string id;

        public int energyCost;

        public string name;
        public string description;
        public string imageSource;

        public Card(string id, int energyCost, string name) {
            this.id = id;

            this.energyCost = energyCost;

            this.name = name;
        }


        public bool CanBuy(Player player) {
            return (player.energyPoint >= energyCost);
        }

        public string Source {
            get { return @"\Images\Cards\" + name + ".jpg"; }

        }
    }
}