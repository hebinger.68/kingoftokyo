﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KingOfTokyo.Game.Cards {
    [Serializable]
    public class CardAction : Card {

        public CardAction(string id, int energyCost, string name)
            : base(id, energyCost, name) {

        }
    }
}
