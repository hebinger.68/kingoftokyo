﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KingOfTokyo.Game.Cards
{
    [Serializable]
    public class CardPower : Card
    {
        public bool isActivate;
        public CardPower(string id, int energyCost, string name)
            : base(id, energyCost, name)
        {
            this.isActivate = false;
        }
    }
}
