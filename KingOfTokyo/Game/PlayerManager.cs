﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KingOfTokyo.Network;
using KingOfTokyo.Network.Messages;
using System.Windows.Threading;
using System.Threading;
using System.ComponentModel;
using System.Windows.Controls;
using KingOfTokyo.Dialogues;
using System.Windows;

namespace KingOfTokyo.Game {
    public sealed class PlayerManager : IReceiver {


        private static readonly Lazy<PlayerManager> lazy =
            new Lazy<PlayerManager>(() => new PlayerManager());
        public static PlayerManager Instance { get { return lazy.Value; } }


        private Lobby lobby;
        private GameWindow gameWindow;
        private TCPServer server;

        public Dictionary<Guid, Player> players = new Dictionary<Guid, Player>();

        public Player me;
        public Player actual;
        public Player KingOfTokyoCity;
        public Player KingOfTokyoBay;
        public bool isBuyingCards;

        public Card [] shop = new Card [3];


        public void SetLobby(Lobby lobby) {
            this.lobby = lobby;
        }
        public void SetGameWindow(GameWindow gameWindow) {
            this.gameWindow = gameWindow;
        }

        private PlayerManager() {
            server = new TCPServer(this, Constants.portClient);
            server.StartObjectReceiver();
        }

        public int GetClientPort() {
            return server.GetPort();
        }

        public object ReceiveMessage(IMessage message) {

            new Thread(delegate () {
                Application.Current.Dispatcher.Invoke(() => {

                    if (message.GetType() == typeof(PlayerJoin)) {
                        PlayerJoinMessage((PlayerJoin)message);

                    } else if (message.GetType() == typeof(StartGame)) {
                        StartGameMessage((StartGame)message);

                    } else if (message.GetType() == typeof(PlayerStartTurn)) {
                        StartTurnMessage((PlayerStartTurn)message);
                    } else if (message.GetType() == typeof(PlayerLaunchDices)) {
                        PlayerLaunchDicesMessage((PlayerLaunchDices)message);
                    } else if (message.GetType() == typeof(UpdateShop)) {
                        UpdateShopMessage((UpdateShop)message);
                    } else if (message.GetType() == typeof(UpdatePlayer)) {
                        UpdatePlayerMessage((UpdatePlayer)message);
                    } else if (message.GetType() == typeof(PlayerStartBuyCard)) {
                        PlayerStartBuyCardMessage((PlayerStartBuyCard)message);
                    } else if (message.GetType() == typeof(EndGame)) {
                        EndGameMessage((EndGame)message);
                    }
                });
            }).Start();

            return 0;
        }

        private void EndGameMessage(EndGame message) {
            gameWindow.labelMessage.Visibility = Visibility.Visible;
            gameWindow.rectangleInfos.Visibility = Visibility.Visible;
            if (message.player != null) {
                gameWindow.labelMessage.Content = "                    Le monstre " + message.player.name + " a gagné !";
            } else {
                gameWindow.labelMessage.Content = "                    Nous n'avons pas de gagnants !";
            }
        }

        private async void PlayerStartBuyCardMessage(PlayerStartBuyCard message) {
            if (message.player == me) {
                isBuyingCards = true;
                gameWindow.buttonEnd.IsEnabled = true; gameWindow.rectangleInfos.Visibility = Visibility.Visible;
                gameWindow.labelMessage.Visibility = Visibility.Visible;
                gameWindow.labelMessage.Content = "                Vous pouvez acheter des cartes";
                await Task.Delay(1500);
                gameWindow.rectangleInfos.Visibility = Visibility.Hidden;
                gameWindow.labelMessage.Visibility = Visibility.Hidden;
                gameWindow.buttonEnd.Content = "Finir tour";
                gameWindow.ShufflePriceImage.Visibility = Visibility.Visible;
                gameWindow.ShufflePriceLabel.Visibility = Visibility.Visible;
                gameWindow.buttonShuffle.Visibility = Visibility.Visible;
            } else {
                gameWindow.rectangleInfos.Visibility = Visibility.Visible;
                gameWindow.labelMessage.Visibility = Visibility.Visible;
                gameWindow.labelMessage.Content = "C'est au monstre " + message.player.name + " d'acheter des cartes";
                await Task.Delay(1500);
                gameWindow.rectangleInfos.Visibility = Visibility.Hidden;
                gameWindow.labelMessage.Visibility = Visibility.Hidden;
            }
        }

        private async void UpdatePlayerMessage(UpdatePlayer message) {
            List<Player> playerChanged = new List<Player>();
            KingOfTokyoCity = null;
            KingOfTokyoBay = null;
            int health = 0;
            int energy = 0;
            int victory = 0;
            foreach (Player player in message.players) {
                if (players [player.id].hasChanged(player)) {
                    playerChanged.Add(player);
                }
                if (player.id == me.id) {
                    me = player;
                    if (gameWindow != null) {
                        gameWindow.UpdatePlayerDisplay(player);
                    }
                }
                if (player.position == PlayerPosition.cityKing) {
                    KingOfTokyoCity = player;
                }
                if (player.position == PlayerPosition.bayKing) {
                    KingOfTokyoBay = player;
                }
                if (gameWindow != null) {
                    gameWindow.UpdatePlayersDisplay(player);
                }
            }
            foreach (Player player in playerChanged) {
                health = -1 * (players [player.id].healthPoint - player.healthPoint);
                energy = -1 * (players [player.id].energyPoint - player.energyPoint);
                victory = -1 * (players [player.id].victoryPoint - player.victoryPoint);
                gameWindow.ChangeInfosPlayers(player, health, energy, victory);
                players [player.id] = player;
            }
            await Task.Delay(3000);
            foreach (Player player in playerChanged) {
                gameWindow.EndChangeInfosPlayers(player);
            }
        }

        private void PlayerLaunchDicesMessage(PlayerLaunchDices message) {
            gameWindow.currentDiceLaunch = message;
            gameWindow.UpdateDiceDisplay();
            gameWindow.UpdateDiceAcces();

        }

        private void UpdateShopMessage(UpdateShop message) {
            this.shop = message.shop;
            if (gameWindow != null) {
                gameWindow.UpdateShop();
            }
        }

        private async void StartTurnMessage(PlayerStartTurn message) {
            actual = message.player;
            if (actual == me) {
                gameWindow.rectangleInfos.Visibility = Visibility.Visible;
                gameWindow.labelMessage.Visibility = Visibility.Visible;
                gameWindow.labelMessage.Content = "                                         Lancez les dés";
                await Task.Delay(1500);
                gameWindow.rectangleInfos.Visibility = Visibility.Hidden;
                gameWindow.labelMessage.Visibility = Visibility.Hidden;
                gameWindow.buttonEnd.Content = "Résoudre dés";
            } else {
                gameWindow.rectangleInfos.Visibility = Visibility.Visible;
                gameWindow.labelMessage.Visibility = Visibility.Visible;
                gameWindow.labelMessage.Content = "C'est au monstre " + message.player.name + " de lancer les dés";
                await Task.Delay(1500);
                gameWindow.rectangleInfos.Visibility = Visibility.Hidden;
                gameWindow.labelMessage.Visibility = Visibility.Hidden;
            }
        }

        private void StartGameMessage(StartGame message) {
            lobby.mainWindow.Main.Content = new GameWindow();
        }

        private void PlayerJoinMessage(PlayerJoin message) {
            Player player = message.player;
            if (player.id == me.id) {
                me = player;
            }

            Console.WriteLine("Received Message : " + player.id + " " + player.name);

            if (!players.ContainsKey(player.id)) {
                players.Add(player.id, player);
                Console.WriteLine("player added");
            }

            if (lobby != null) {
                List<Label> labelsNom = new List<Label>() { lobby.labelNom1, lobby.labelNom2, lobby.labelNom3, lobby.labelNom4, lobby.labelNom5, lobby.labelNom6 };

                for (int i = 0; i < players.Count; i++) {
                    AfficheLobbyMessageLabel(players.ToList() [i].Value.name, labelsNom [i]);
                    //AfficheLobbyMessageLabel(player.IP, lobby.labelIp1);
                }
            }
        }

        private void AfficheLobbyMessageLabel(string text, Label label) {

            lobby.Affiche(text, label);

        }


        public void SendStartGame() {
            StartGame message = new StartGame();
            Network.TCPClient.SendObject(message, "127.0.0.1", Constants.portServer);
        }

        public void checkBuyCard(Card card) {
            if (actual == me && isBuyingCards) {
                if (card.CanBuy(me)) {
                    PlayerBoughtCard message = new PlayerBoughtCard() { player = me, cardId = card.id };
                    Network.TCPClient.SendObject(message, "127.0.0.1", Constants.portServer);
                } else {
                    Console.WriteLine("Can not Buy");
                }
            } else {
                Console.WriteLine("Can not Buy now");
            }
        }

        public void checkShuffleShop() {
            if (actual == me && isBuyingCards) {
                if (me.energyPoint >= 2) {
                    PlayerShuffleShop message = new PlayerShuffleShop() { player = me };
                    Network.TCPClient.SendObject(message, "127.0.0.1", Constants.portServer);
                } else {
                    Console.WriteLine("Can not Shuffle");
                }
            } else {
                Console.WriteLine("Can not Shuffle now");
            }
        }

        internal void sendPlayerLaunchDicesMessage(PlayerLaunchDices currentDiceLaunch) {
            Network.TCPClient.SendObject(currentDiceLaunch, "127.0.0.1", Constants.portServer);
        }

        internal void sendPlayerEndTurnMessage() {
            PlayerEndTurn message = new PlayerEndTurn() { player = me };
            Network.TCPClient.SendObject(message, "127.0.0.1", Constants.portServer);
        }

        internal void sendPlayerWantToLeaveMessage() {
            PlayerWantToLeave message = new PlayerWantToLeave() { player = me };
            Network.TCPClient.SendObject(message, "127.0.0.1", Constants.portServer);
        }

        internal void sendPlayerActivatePowerCard(string CardId) {
            PlayerActivatePowerCard message = new PlayerActivatePowerCard() { player = me, cardId = CardId };
            Network.TCPClient.SendObject(message, "127.0.0.1", Constants.portServer);
        }
    }
}
