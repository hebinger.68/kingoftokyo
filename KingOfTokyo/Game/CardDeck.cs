﻿using KingOfTokyo.Game.Cards;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KingOfTokyo.Game {
    public class CardDeck {

        private static readonly Lazy<CardDeck> lazy =
            new Lazy<CardDeck>(() => new CardDeck());
        public static CardDeck Instance { get { return lazy.Value; } }

        private List<Card> Deck;

        public List<Card> GetCards() {
            return new List<Card>(Deck);
        }

        public CardDeck() {
            Deck = new List<Card>();

            // Action Card
            Deck.Add(new CardAction(Deck.Count.ToString(), 5, "AvionsDeChasse"));
            Deck.Add(new CardAction(Deck.Count.ToString(), 4, "BombardementAerien"));
            Deck.Add(new CardAction(Deck.Count.ToString(), 6, "CentraleNucleaire"));
            Deck.Add(new CardAction(Deck.Count.ToString(), 3, "GardeNationale"));
            Deck.Add(new CardAction(Deck.Count.ToString(), 6, "GratteCiel"));
            Deck.Add(new CardAction(Deck.Count.ToString(), 5, "Immeuble"));
            Deck.Add(new CardAction(Deck.Count.ToString(), 5, "LaMortVientDuCiel"));
            Deck.Add(new CardAction(Deck.Count.ToString(), 3, "LanceFlammes"));
            Deck.Add(new CardAction(Deck.Count.ToString(), 3, "LaPetiteBoutiqueDuCoin"));
            Deck.Add(new CardAction(Deck.Count.ToString(), 7, "OrdreDevacuation"));
            Deck.Add(new CardAction(Deck.Count.ToString(), 6, "RaffinerieDeGaz"));
            Deck.Add(new CardAction(Deck.Count.ToString(), 7, "Rage"));
            Deck.Add(new CardAction(Deck.Count.ToString(), 8, "Recharge"));
            Deck.Add(new CardAction(Deck.Count.ToString(), 3, "Soin"));
            Deck.Add(new CardAction(Deck.Count.ToString(), 4, "Tank"));
            Deck.Add(new CardAction(Deck.Count.ToString(), 6, "TempeteMagnetique"));
            Deck.Add(new CardAction(Deck.Count.ToString(), 4, "Tramway"));

            // Power Card
            Deck.Add(new CardPower(Deck.Count.ToString(), 6, "Ailes"));
            Deck.Add(new CardPower(Deck.Count.ToString(), 4, "ArmureDePlaques"));
            Deck.Add(new CardPower(Deck.Count.ToString(), 6, "AttaqueAcide"));
            Deck.Add(new CardPower(Deck.Count.ToString(), 4, "BoissonEnergisante"));
            Deck.Add(new CardPower(Deck.Count.ToString(), 5, "CerveauDemesure"));
            Deck.Add(new CardPower(Deck.Count.ToString(), 3, "DardsEmpoisonnes"));
            Deck.Add(new CardPower(Deck.Count.ToString(), 3, "DestructionTotale"));
            Deck.Add(new CardPower(Deck.Count.ToString(), 4, "Detritivore"));
            Deck.Add(new CardPower(Deck.Count.ToString(), 4, "EncorePlusGrand"));
            Deck.Add(new CardPower(Deck.Count.ToString(), 2, "EnergieSolaire"));
            Deck.Add(new CardPower(Deck.Count.ToString(), 5, "Fouisseur"));
            Deck.Add(new CardPower(Deck.Count.ToString(), 4, "Gourmet"));
            Deck.Add(new CardPower(Deck.Count.ToString(), 3, "GuerisonRapide"));
            Deck.Add(new CardPower(Deck.Count.ToString(), 7, "IlRestaitUnOeuf"));
            Deck.Add(new CardPower(Deck.Count.ToString(), 3, "LAmiDesEnfants"));
            Deck.Add(new CardPower(Deck.Count.ToString(), 3, "Mediatique"));
            Deck.Add(new CardPower(Deck.Count.ToString(), 5, "MonstreAlpha"));
            Deck.Add(new CardPower(Deck.Count.ToString(), 4, "Necrophage"));
            Deck.Add(new CardPower(Deck.Count.ToString(), 3, "NousNeFaisonsQueLeRendrePlusFort"));
            Deck.Add(new CardPower(Deck.Count.ToString(), 4, "NinjaUrbain"));
            Deck.Add(new CardPower(Deck.Count.ToString(), 3, "OrigineExtraTerrestre"));
            Deck.Add(new CardPower(Deck.Count.ToString(), 5, "QueueAPiquants"));
            Deck.Add(new CardPower(Deck.Count.ToString(), 4, "Regeneration"));
            Deck.Add(new CardPower(Deck.Count.ToString(), 3, "ReserveDenergie"));
            Deck.Add(new CardPower(Deck.Count.ToString(), 7, "SouffleNova"));
            Deck.Add(new CardPower(Deck.Count.ToString(), 3, "TropMignon"));
            Deck.Add(new CardPower(Deck.Count.ToString(), 7, "UneTeteDePlus"));
            Deck.Add(new CardPower(Deck.Count.ToString(), 7, "UneTeteDePlus"));
            Deck.Add(new CardPower(Deck.Count.ToString(), 4, "Urbavore"));

        }

    }
}
