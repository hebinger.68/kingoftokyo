﻿using System;
using System.Collections.Generic;

namespace KingOfTokyo.Game {
    [Serializable]
    public class Dices {

        public Dices(int bonusCount) {
            list = new List<Dice>();
            for (int i = 0; i < 6; i++) {
                list.Add(new Dice() { face = DiceFaces.none, isPlayable = true, isSelected = false, isUsed = true });
            }

            //bonus
            for (int i = 0; i < bonusCount; i++) {
                list.Add(new Dice() { face = DiceFaces.none, isPlayable = true, isSelected = false, isUsed = true });
            }
            for (int i = bonusCount; i < 2; i++) {
                list.Add(new Dice() { face = DiceFaces.none, isPlayable = false, isSelected = false, isUsed = false });
            }

        }

        public List<Dice> list;


        [Serializable]
        public class Dice {
            public DiceFaces face;
            public bool isPlayable;
            public bool isSelected;
            public bool isUsed;
        }

    }
    [Serializable]
    public enum DiceFaces {
        none,
        one,
        two,
        three,
        hurt,
        energy,
        heal
    }
}