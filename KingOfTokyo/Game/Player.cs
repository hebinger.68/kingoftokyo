﻿using KingOfTokyo.Game.Cards;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KingOfTokyo.Game {
    [Serializable]
    public class Player {

        public Guid id;
        public string name;
        public string ip;
        public int port;
        public bool isHost;
        public bool canLeave;
        public bool isAlive;

        public int victoryPoint { get; private set; }
        public int healthPoint;
        public int energyPoint;
        public int maxHealth = 10;

        public string monsterName = "";

        public List<CardPower> cards = new List<CardPower>();
        public PlayerPosition position;


        public static bool operator ==(Player p1, Player p2) {
            if (p1 is null && p2 is null) return true;
            if (p1 is null || p2 is null) return false;
            return (p1.id == p2.id);
        }

        public static bool operator !=(Player p1, Player p2) {
            return !(p1 == p2);
        }

        public void ChangeVictoryPoint(int points) {
            if (victoryPoint + points < 0) {
                victoryPoint = 0;
            } else {
                victoryPoint += points;
            }
        }

        public void ChangeEnergyPoint(int energy)
        {
            if (energyPoint + energy < 0)
            {
                energyPoint = 0;
            }
            else
            {
                energyPoint += energy;
            }
        }

        public bool HasCard(string cardName)
        {
            return this.cards.Where(c => c.name.Equals(cardName)).Any();
        }

        public void SetVictoryPoint(int points) {
            victoryPoint = points;
        }

        public bool hasChanged(Player player) {
            if (player.healthPoint != this.healthPoint) {
                return true;
            }
            if (player.energyPoint != this.energyPoint) {
                return true;
            }
            if (player.victoryPoint != this.victoryPoint) {
                return true;
            }
            if (player.cards != this.cards) {
                return true;
            }
            if (player.position != this.position) {
                return true;
            }
            return false;
        }

        internal void heal(int health) {
            if (this.healthPoint + health < maxHealth) {
                this.healthPoint += health;
            } else {
                this.healthPoint = maxHealth;
            }
        }

        internal void hurt(int hurt) {
            foreach (CardPower card in this.cards)
            {
                if (card.name.Equals("Ailes") && card.isActivate)
                {
                    return;
                }
                if (card.name.Equals("ArmureDePlaques") && hurt == 1)
                {
                    return;
                }
                if (card.name.Equals("NousNeFaisonsQueLeRendrePlusFort") && hurt >= 2)
                {
                    this.ChangeEnergyPoint(1);
                }
            }
            if (this.healthPoint - hurt > 0)
            {
                this.healthPoint -= hurt;
            }
            else
            {
                this.healthPoint = 0;
            }
        }

        internal bool hasLooseHealth(Player player) {
            if (this.healthPoint > player.healthPoint) {
                return true;
            }
            return false;
        }

        internal bool IsInCity {
            get {
                return (position != PlayerPosition.outside);
            }
        }

        internal bool HasEnoughEnergy(int energy) {
            return (this.energyPoint >= energy);
        }

    }
    [Serializable]
    public enum PlayerPosition {
        cityKing,
        bayKing,
        outside
    }
}
