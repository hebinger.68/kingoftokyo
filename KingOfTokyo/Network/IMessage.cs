﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace KingOfTokyo.Network {

    public interface IMessage {

        Guid MessageTarget { get; set; }

    }
}
