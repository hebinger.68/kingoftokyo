﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using KingOfTokyo.Network.Messages;

namespace KingOfTokyo.Network {
    class TCPServer {
        private static Thread _thEcoute;
        private IReceiver receiver;
        private int port;

        private IPEndPoint ep;

        public TCPServer(IReceiver receiver, int port) {
            this.receiver = receiver;
            if (port == 0) {
                this.port = FreeTcpPort();
                Console.WriteLine("Client port : " + this.port);
            } else {
                this.port = port;
            }
        }

        public void StartObjectReceiver() {
            _thEcoute = new Thread(ObjectReceiver);
            _thEcoute.Start();
        }

        public int GetPort() {
            return this.ep.Port;
        }


        public void ObjectReceiver() {
            //pas ouf, il faudrait refactor mais pour le moment ca marche.

            ep = new IPEndPoint(IPAddress.Loopback, this.port);
            TcpListener listener = new TcpListener(ep);
            listener.Start();
            // Run the loop continously; this is the server.
            while (true) {
                Console.WriteLine("test 1");

                TcpClient client = listener.AcceptTcpClient();
                NetworkStream stream = client.GetStream();

                // Read the message, and perform different actions
                //message = cleanMessage(buffer);

                IFormatter formatter = new BinaryFormatter();

                IMessage messageObject = (IMessage)formatter.Deserialize(stream);

                receiver.ReceiveMessage(messageObject);


                //Response message for the client
                byte [] bytes = Encoding.Unicode.GetBytes("Thank you for your message, ");
                client.GetStream().Write(bytes, 0, bytes.Length); // Send the response
            }
        }


        static int FreeTcpPort() {
            TcpListener l = new TcpListener(IPAddress.Loopback, 0);
            l.Start();
            int port = ((IPEndPoint)l.LocalEndpoint).Port;
            l.Stop();
            return port;
        }


    }
}
