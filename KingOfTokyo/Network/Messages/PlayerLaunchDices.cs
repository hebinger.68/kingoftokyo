﻿using KingOfTokyo.Game;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KingOfTokyo.Network.Messages {
    [Serializable]
    public class PlayerLaunchDices : IMessage {
        public Player player;
        public Dices dices;


        public bool hasFinished;

        public int launchCount;


        public Guid MessageTarget { get; set; }
    }
}
