﻿using KingOfTokyo.Game;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KingOfTokyo.Network.Messages
{
    [Serializable]
    class PlayerShuffleShop : IMessage
    {
        public Player player;

        public Guid MessageTarget { get; set; }
    }
}
