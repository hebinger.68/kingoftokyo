﻿using KingOfTokyo.Game;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KingOfTokyo.Network.Messages {
    [Serializable]
    public class StartGame : IMessage {


        public Guid MessageTarget { get; set; }
    }
}
