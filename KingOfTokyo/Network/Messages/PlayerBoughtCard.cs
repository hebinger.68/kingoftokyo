﻿using KingOfTokyo.Game;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KingOfTokyo.Network.Messages {
    [Serializable]
    public class PlayerBoughtCard : IMessage {
        public Player player;
        public string cardId;

        public Guid MessageTarget { get; set; }
    }
}
