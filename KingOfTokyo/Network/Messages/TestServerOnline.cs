﻿using KingOfTokyo.Game;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KingOfTokyo.Network.Messages
{
    [Serializable]
    class TestServerOnline : IMessage
    {
        public bool online = false;
        public bool full = false;
        public string ip;
        public int port;
        public Guid MessageTarget { get; set; }
    }
}
