﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace KingOfTokyo.Network {
    public static class TCPClient
    //TODO mettre en multithreading
    {

        public static void SendObject(IMessage message, string ip, int port) {
            const int bytesize = 1024;
            byte [] response = new byte [bytesize];
            try {
                TcpClient client = new TcpClient(ip, port); // Create a new connection
                NetworkStream stream = client.GetStream();

                //stream.Write(messageBytes, 0, messageBytes.Length); // Write the bytes
                IFormatter formatter = new BinaryFormatter();
                formatter.Serialize(stream, message);

                //reception de la reponse TCP
                stream.Read(response, 0, response.Length);

                //Console.WriteLine("Reponse client : " + Encoding.Unicode.GetString(response,0,response.Length)); fait crach

                // Clean up
                stream.Dispose();
                client.Close();

            } catch (Exception e) {
                Console.WriteLine(e.Message);
            }
        }


        public static void SendMessage(string message, string ip, int port) {
            const int bytesize = 1024 * 1024;
            byte [] messageBytes = System.Text.Encoding.Unicode.GetBytes(message);
            try {
                TcpClient client = new TcpClient(ip, port); // Create a new connection
                NetworkStream stream = client.GetStream();

                stream.Write(messageBytes, 0, messageBytes.Length); // Write the bytes

                messageBytes = new byte [bytesize];

                stream.Read(messageBytes, 0, messageBytes.Length);



                // Clean up
                stream.Dispose();
                client.Close();
            } catch (Exception e) {
                Console.WriteLine(e.Message);
            }

        }



    }
}
