﻿using KingOfTokyo.Network;
using KingOfTokyo.Network.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KingOfTokyo.Dialogues {
    /// <summary>
    /// Logique d'interaction pour Home.xaml
    /// </summary>
    public partial class Home : Page, IReceiver {

        private MainWindow mainWindow;
        private bool serverOnline = false;
        private bool serverFull = false;
        private TCPServer server;

        public Home(MainWindow mainWindow) {
            this.mainWindow = mainWindow;
            InitializeComponent();
            server = new TCPServer(this, Constants.portClient);
            server.StartObjectReceiver();
        }

        public object ReceiveMessage(IMessage message)
        {
            if (message.GetType() == typeof(TestServerOnline))
            {
                TestServerOnlineMessage((TestServerOnline)message);
            }
            return 0;
        }

        private void TestServerOnlineMessage(TestServerOnline message)
        {
            serverOnline = message.online;
            serverFull = message.full;
        }

        private void ButtonLaunchClick(object sender, RoutedEventArgs e) {
            TestServerOnline message = new TestServerOnline() { ip = "127.0.0.1", port = server.GetPort() };
            Network.TCPClient.SendObject(message, "127.0.0.1", Constants.portServer);
            if (serverOnline && !serverFull)
            {
                Dialogues.Lobby lobby = new Lobby(GetName(), false, mainWindow);//TODO connexion à une ip différente
                mainWindow.Main.Content = lobby;
            }
            else
            {
                labelMessage.Visibility = Visibility.Visible;
                labelMessage.Content = "Impossible de rejoindre";
            }
            //this.Close();
        }

        private void ButtonCreateClick(object sender, RoutedEventArgs e) {
            TestServerOnline message = new TestServerOnline() { ip = "127.0.0.1", port = server.GetPort() };
            Network.TCPClient.SendObject(message, "127.0.0.1", Constants.portServer);
            if (!serverOnline)
            {
                Dialogues.Lobby lobby = new Lobby(GetName(), true, mainWindow);
                mainWindow.Main.Content = lobby;
            }
            else
            {
                labelMessage.Visibility = Visibility.Visible;
                labelMessage.Content = "Impossible de créer";
            }
            //this.Close();

        }

        public string GetName() {
            if (textBoxPlayerName.Text == string.Empty) {
                return "King" + new Random().Next(10000);
            }
            return textBoxPlayerName.Text;

        }
    }
}
