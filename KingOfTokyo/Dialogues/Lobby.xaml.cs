﻿using KingOfTokyo.Game;
using KingOfTokyo.Network.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace KingOfTokyo.Dialogues {
    /// <summary>
    /// Logique d'interaction pour Lobby.xaml
    /// </summary>
    public partial class Lobby : Page {
        public PlayerManager playerManager = PlayerManager.Instance;
        private bool isHost;
        private string playerName;

        Server.Server server;

        public MainWindow mainWindow;

        public Lobby(string playerName, bool isHost, MainWindow mainWindow) {
            this.mainWindow = mainWindow;
            this.playerName = playerName;
            this.isHost = isHost;

            InitializeComponent();

            if (isHost) {
                this.server = new Server.Server();
            } else {
                this.buttonStartGame.Visibility = Visibility.Hidden;
            }

            playerManager.SetLobby(this);
            joinServer();
        }


        public void joinServer() {
            Player player = new Player() { id = Guid.NewGuid(), name = playerName, ip = "127.0.0.1", port = playerManager.GetClientPort(), isHost = isHost };
            PlayerJoin message = new PlayerJoin() { player = player };

            playerManager.me = player;

            Network.TCPClient.SendObject(message, "127.0.0.1", Constants.portServer);
        }

        internal void Affiche(string text, Label label) {
            label.Content = text;
        }

        private void Window_Closed(object sender, EventArgs e) {

        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e) {
            //Environment.Exit(0);
        }

        private void buttonStartGame_Click(object sender, RoutedEventArgs e) {
            if (!isHost) return;
            playerManager.SendStartGame();
        }


    }
}
