﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using KingOfTokyo.Game;
using KingOfTokyo.Network.Messages;

namespace KingOfTokyo.Dialogues {
    /// <summary>
    /// Logique d'interaction pour GameWindow.xaml
    /// </summary>
    public partial class GameWindow : Page {
        public PlayerManager playerManager = PlayerManager.Instance;


        //private Dictionary<string, string> playerDisplays = new Dictionary<string, string>();
        private List<PlayerStatPopup> popUps = new List<PlayerStatPopup>();
        private Dictionary<Guid, PlayerStatPopup> popUpPlayer = new Dictionary<Guid, PlayerStatPopup>();

        private Card [] shop;
        private List<Image> diceButtons = new List<Image>();



        public GameWindow() {
            InitializeComponent();
            //this.WindowState = WindowState.Maximized;
            playerManager.SetGameWindow(this);

            popUps.Add(popUpPlayer1);
            popUps.Add(popUpPlayer2);
            popUps.Add(popUpPlayer3);
            popUps.Add(popUpPlayer4);
            popUps.Add(popUpPlayer5);
            popUps.Add(popUpPlayer6);

            foreach (PlayerStatPopup popup in popUps) {
                popup.Visibility = Visibility.Hidden;
            }

            ShufflePriceImage.Visibility = Visibility.Hidden;
            ShufflePriceLabel.Visibility = Visibility.Hidden;
            buttonShuffle.Visibility = Visibility.Hidden;

            diceButtons.Add(imageDice1);
            diceButtons.Add(imageDice2);
            diceButtons.Add(imageDice3);
            diceButtons.Add(imageDice4);
            diceButtons.Add(imageDice5);
            diceButtons.Add(imageDice6);

            diceButtons.Add(imageDice7);
            diceButtons.Add(imageDice8);

            buttonStartDices.IsEnabled = false;
            buttonEnd.IsEnabled = false;
            CardPlayer1.Source = null;
            CardPlayer2.Source = null;
            CardPlayer3.Source = null;
            imageKingCity.Source = null;
            imageKingBay.Source = null;
            foreach (Image dice in diceButtons) {
                dice.IsEnabled = false;
            }

            for (int i = 0; i < playerManager.players.Select(d => d.Value).Count(); i++) {
                Player player = playerManager.players.Select(d => d.Value).ToList() [i];
                //playerDisplays.Add(player.playerID, "labelPlayer" + (i + 1));
                popUpPlayer.Add(player.id, popUps [i]);
                UpdatePlayersDisplay(player);
            }
            UpdatePlayerDisplay(playerManager.players [playerManager.me.id]);
            UpdateShop();
        }

        public void UpdateShop() {
            shop = playerManager.shop;
            Card1.Source = new BitmapImage(new Uri(shop [0].Source, UriKind.Relative));
            Card2.Source = new BitmapImage(new Uri(shop [1].Source, UriKind.Relative));
            Card3.Source = new BitmapImage(new Uri(shop [2].Source, UriKind.Relative));
        }

        public void UpdatePlayersDisplay(Player player) {
            PlayerStatPopup popup = popUpPlayer [player.id];
            popup.labelPlayerName.Content = player.name;
            popup.labelEnergy.Content = player.energyPoint;
            popup.labelHealth.Content = player.healthPoint;
            popup.labelVictory.Content = player.victoryPoint;
            popup.imageMonster.Source = new BitmapImage(new Uri(@"\Images\Monsters\" + player.monsterName + ".png", UriKind.Relative));
            popup.BackgroundImage.Source = new BitmapImage(new Uri(@"\Images\Monsters\" + player.monsterName + "-Card" + ".png", UriKind.Relative));
            if (player.cards != null) {
                popup.cardImage1.Source = null;
                popup.cardImage2.Source = null;
                popup.cardImage3.Source = null;
                if (player.cards.Count() >= 1) {
                    popup.cardImage1.Source = new BitmapImage(new Uri(player.cards [0].Source, UriKind.Relative));
                }
                if (player.cards.Count() >= 2) {
                    popup.cardImage2.Source = new BitmapImage(new Uri(player.cards [1].Source, UriKind.Relative));
                }
                if (player.cards.Count() >= 3) {
                    popup.cardImage3.Source = new BitmapImage(new Uri(player.cards [2].Source, UriKind.Relative));
                }
            }
            if (popup.Visibility != Visibility.Visible) {
                popup.Visibility = Visibility.Visible;
            }
            if (playerManager.KingOfTokyoCity == null) {
                labelKingOfTokyo.Content = "";
                imageKingCity.Source = null;
            }
            if (playerManager.KingOfTokyoBay == null) {
                labelKingOfTokyoBay.Content = "";
                imageKingBay.Source = null;
            }
            if (player == playerManager.KingOfTokyoCity) {
                labelKingOfTokyo.Content = player.name;
                imageKingCity.Source = new BitmapImage(new Uri(@"\Images\Monsters\" + player.monsterName + ".png", UriKind.Relative));
            } else if (player == playerManager.KingOfTokyoBay) {
                labelKingOfTokyoBay.Content = player.name;
                imageKingBay.Source = new BitmapImage(new Uri(@"\Images\Monsters\" + player.monsterName + ".png", UriKind.Relative));
            }

            if (!player.isAlive) {
                popup.labelPlayerName.Background = new SolidColorBrush(new Color() { R = 250, G = 0, B = 0, A = 255 });
            }

        }

        public void ChangeInfosPlayers(Player player, int health = 0, int energy = 0, int victory = 0) {
            PlayerStatPopup popup = popUpPlayer [player.id];
            if (health != 0) {
                popup.labelChangeHealth.Visibility = Visibility.Visible;
                if (health > 0) {
                    popup.labelChangeHealth.Content = "+" + health;
                } else {
                    popup.labelChangeHealth.Content = health;
                }
                popup.imageChangeHealth.Visibility = Visibility.Visible;
            }
            if (energy != 0) {
                popup.labelChangeEnergy.Visibility = Visibility.Visible;
                if (energy > 0) {
                    popup.labelChangeEnergy.Content = "+" + energy;
                } else {
                    popup.labelChangeEnergy.Content = energy;
                }
                popup.imageChangeEnergy.Visibility = Visibility.Visible;
            }
            if (victory != 0) {
                popup.labelChangeVictory.Visibility = Visibility.Visible;
                if (victory > 0) {
                    popup.labelChangeVictory.Content = "+" + victory;
                } else {
                    popup.labelChangeVictory.Content = victory;
                }
                popup.imageChangeVictory.Visibility = Visibility.Visible;
            }
        }

        public void EndChangeInfosPlayers(Player player) {
            PlayerStatPopup popup = popUpPlayer [player.id];
            popup.labelChangeHealth.Visibility = Visibility.Hidden;
            popup.imageChangeHealth.Visibility = Visibility.Hidden;
            popup.labelChangeEnergy.Visibility = Visibility.Hidden;
            popup.imageChangeEnergy.Visibility = Visibility.Hidden;
            popup.labelChangeVictory.Visibility = Visibility.Hidden;
            popup.imageChangeVictory.Visibility = Visibility.Hidden;
        }

        public void UpdatePlayerDisplay(Player player) {
            labelNameMonster.Content = player.name;
            labelEnergyPoints.Content = player.energyPoint;
            labelLifeValuePoints.Content = player.healthPoint;
            labelVictoryPoints.Content = player.victoryPoint;
            imageMonster.Source = new BitmapImage(new Uri(@"\Images\Monsters\" + player.monsterName + "-Card" + ".png", UriKind.Relative));
            if (player.cards != null) {
                if (player.cards.Count() == 1) {
                    CardPlayer1.Source = new BitmapImage(new Uri(player.cards [0].Source, UriKind.Relative));
                }
                if (player.cards.Count() == 2) {
                    CardPlayer2.Source = new BitmapImage(new Uri(player.cards [1].Source, UriKind.Relative));
                }
                if (player.cards.Count() == 3) {
                    CardPlayer3.Source = new BitmapImage(new Uri(player.cards [2].Source, UriKind.Relative));
                }
            }

        }


        public PlayerLaunchDices currentDiceLaunch;
        public void UpdateDiceDisplay() {
            for (int i = 0; i < 8; i++) {
                UpdateDiceFace(diceButtons [i], currentDiceLaunch.dices.list [i]);
            }
        }

        public void UpdateDiceAcces() {

            if (currentDiceLaunch.player != playerManager.me || currentDiceLaunch.hasFinished) {
                buttonStartDices.Visibility = Visibility.Hidden;
                buttonStartDices.IsEnabled = false;
                buttonEnd.IsEnabled = false;
                return;
            }

            if (currentDiceLaunch.launchCount == 0) {
                buttonEnd.IsEnabled = false;
                foreach (Image button in diceButtons) {
                    button.IsEnabled = false;
                }
                foreach (Dices.Dice dice in currentDiceLaunch.dices.list.Where(d => d.isUsed)) {
                    dice.isSelected = true;
                }
            } else {
                buttonEnd.IsEnabled = true;
                foreach (Dices.Dice dice in currentDiceLaunch.dices.list) {
                    dice.isSelected = false;
                }
                for (int i = 0; i < 8; i++) {
                    diceButtons [i].IsEnabled = currentDiceLaunch.dices.list [i].isPlayable;
                }
            }

            if (currentDiceLaunch.launchCount == 0 || currentDiceLaunch.dices.list.Where(d => d.isPlayable).Any() && currentDiceLaunch.dices.list.Where(d => d.isSelected).Any()) {
                buttonStartDices.Visibility = Visibility.Visible;
                buttonStartDices.IsEnabled = true;
            } else {
                buttonStartDices.IsEnabled = false;
                buttonStartDices.Visibility = Visibility.Hidden;
            }

            UpdateDiceDisplay();


        }

        private void UpdateDiceFace(Image button, Dices.Dice dice) {
            if (!dice.isUsed) {
                button.Visibility = Visibility.Hidden;
                return;
            }
            button.Visibility = Visibility.Visible;
            switch (dice.face) {
                case DiceFaces.none:
                    button.Source = new BitmapImage(new Uri(@"\Images\Dices\dice-none-2.png", UriKind.Relative));
                    break;
                case DiceFaces.one:
                    button.Source = new BitmapImage(new Uri(@"\Images\Dices\dice-one-2.png", UriKind.Relative));
                    break;
                case DiceFaces.two:
                    button.Source = new BitmapImage(new Uri(@"\Images\Dices\dice-two-2.png", UriKind.Relative));
                    break;
                case DiceFaces.three:
                    button.Source = new BitmapImage(new Uri(@"\Images\Dices\dice-three-2.png", UriKind.Relative));
                    break;
                case DiceFaces.energy:
                    button.Source = new BitmapImage(new Uri(@"\Images\Dices\dice-energy-2.png", UriKind.Relative));
                    break;
                case DiceFaces.hurt:
                    button.Source = new BitmapImage(new Uri(@"\Images\Dices\dice-hurt-2.png", UriKind.Relative));
                    break;
                case DiceFaces.heal:
                    button.Source = new BitmapImage(new Uri(@"\Images\Dices\dice-heal-2.png", UriKind.Relative));
                    break;
                default:
                    break;
            }
        }


        private void Card1_MouseUp(object sender, MouseButtonEventArgs e) {
            playerManager.checkBuyCard(shop [0]);
        }

        private void Card2_MouseUp(object sender, MouseButtonEventArgs e) {
            playerManager.checkBuyCard(shop [1]);
        }

        private void Card3_MouseUp(object sender, MouseButtonEventArgs e) {
            playerManager.checkBuyCard(shop [2]);
        }




        private void ButtonStartDices_Click(object sender, RoutedEventArgs e) {

            foreach (Image button in diceButtons) {
                //button.IsEnabled = false;
            }

            playerManager.sendPlayerLaunchDicesMessage(currentDiceLaunch);
        }

        private void ButtonEnd_Click(object sender, RoutedEventArgs e) {
            if (!currentDiceLaunch.hasFinished && currentDiceLaunch.player == playerManager.me) {
                foreach (Image button in diceButtons) {
                    button.IsEnabled = false;
                }
                currentDiceLaunch.hasFinished = true;

                playerManager.sendPlayerLaunchDicesMessage(currentDiceLaunch);

            } else if (playerManager.isBuyingCards) {
                playerManager.isBuyingCards = false;
                ShufflePriceImage.Visibility = Visibility.Hidden;
                ShufflePriceLabel.Visibility = Visibility.Hidden;
                buttonShuffle.Visibility = Visibility.Hidden;
                playerManager.sendPlayerEndTurnMessage();

            }

            buttonEnd.IsEnabled = false;
        }

        private void ButtonLeaveTokyo_Click(object sender, RoutedEventArgs e) {
            playerManager.sendPlayerWantToLeaveMessage();
        }

        private void CardPlayer1_MouseUp(object sender, MouseButtonEventArgs e) {
            ActivateCard(0);
        }

        private void CardPlayer2_MouseUp(object sender, MouseButtonEventArgs e) {
            ActivateCard(1);
        }

        private void CardPlayer3_MouseUp(object sender, MouseButtonEventArgs e) {
            ActivateCard(2);
        }

        private void ActivateCard(int cardNumber) {
            if (playerManager.me.cards [cardNumber] == null) {
                return;
            }
            if (playerManager.me.cards [cardNumber].name == "Ailes" || playerManager.me.cards [cardNumber].name == "BoissonEnergisante" || playerManager.me.cards [cardNumber].name == "GuerisonRapide") {
                playerManager.sendPlayerActivatePowerCard(playerManager.me.cards [cardNumber].id);
            }
        }

        private void Card1_MouseEnter(object sender, MouseEventArgs e) {
            ShopCardHover(e.Source as Image);
        }

        private void Card2_MouseEnter(object sender, MouseEventArgs e) {
            ShopCardHover(e.Source as Image);
        }

        private void Card3_MouseEnter(object sender, MouseEventArgs e) {
            ShopCardHover(e.Source as Image);
        }

        public void ShopCardHover(Image i) {
            Canvas parent = ((Canvas)i.Parent);
            parent.Children.Remove(i);
            parent.Children.Add(i);
        }


        private void Shop_MouseLeave(object sender, MouseEventArgs e) {
            Canvas parent = e.Source as Canvas;
            parent.Children.Remove(Card3);
            parent.Children.Add(Card3);
            parent.Children.Remove(Card2);
            parent.Children.Add(Card2);
            parent.Children.Remove(Card1);
            parent.Children.Add(Card1);


        }

        private void ButtonShuffle_Click(object sender, RoutedEventArgs e) {
            playerManager.checkShuffleShop();
        }

        private void ImageDice1_MouseUp(object sender, MouseButtonEventArgs e) {
            ((Image)e.Source).IsEnabled = false;
            ((Image)e.Source).Visibility = Visibility.Hidden;
            currentDiceLaunch.dices.list [0].isSelected = true;
            buttonStartDices.IsEnabled = true;
            buttonStartDices.Visibility = Visibility.Visible;
            buttonEnd.IsEnabled = true;

        }

        private void ImageDice2_MouseUp(object sender, MouseButtonEventArgs e) {
            ((Image)e.Source).IsEnabled = false;
            ((Image)e.Source).Visibility = Visibility.Hidden;
            currentDiceLaunch.dices.list [1].isSelected = true;
            buttonStartDices.IsEnabled = true;
            buttonStartDices.Visibility = Visibility.Visible;
            buttonEnd.IsEnabled = true;
        }

        private void ImageDice3_MouseUp(object sender, MouseButtonEventArgs e) {
            ((Image)e.Source).IsEnabled = false;
            ((Image)e.Source).Visibility = Visibility.Hidden;
            currentDiceLaunch.dices.list [2].isSelected = true;
            buttonStartDices.IsEnabled = true;
            buttonStartDices.Visibility = Visibility.Visible;
            buttonEnd.IsEnabled = true;
        }

        private void ImageDice4_MouseUp(object sender, MouseButtonEventArgs e) {
            ((Image)e.Source).IsEnabled = false;
            ((Image)e.Source).Visibility = Visibility.Hidden;
            currentDiceLaunch.dices.list [3].isSelected = true;
            buttonStartDices.IsEnabled = true;
            buttonStartDices.Visibility = Visibility.Visible;
            buttonEnd.IsEnabled = true;
        }

        private void ImageDice5_MouseUp(object sender, MouseButtonEventArgs e) {
            ((Image)e.Source).IsEnabled = false;
            ((Image)e.Source).Visibility = Visibility.Hidden;
            currentDiceLaunch.dices.list [4].isSelected = true;
            buttonStartDices.IsEnabled = true;
            buttonStartDices.Visibility = Visibility.Visible;
            buttonEnd.IsEnabled = true;
        }

        private void ImageDice6_MouseUp(object sender, MouseButtonEventArgs e) {
            ((Image)e.Source).IsEnabled = false;
            ((Image)e.Source).Visibility = Visibility.Hidden;
            currentDiceLaunch.dices.list [5].isSelected = true;
            buttonStartDices.IsEnabled = true;
            buttonStartDices.Visibility = Visibility.Visible;
            buttonEnd.IsEnabled = true;
        }

        private void ImageDice7_MouseUp(object sender, MouseButtonEventArgs e) {
            ((Image)e.Source).IsEnabled = false;
            ((Image)e.Source).Visibility = Visibility.Hidden;
            currentDiceLaunch.dices.list [6].isSelected = true;
            buttonStartDices.IsEnabled = true;
            buttonStartDices.Visibility = Visibility.Visible;
            buttonEnd.IsEnabled = true;
        }

        private void ImageDice8_MouseUp(object sender, MouseButtonEventArgs e) {
            ((Image)e.Source).IsEnabled = false;
            ((Image)e.Source).Visibility = Visibility.Hidden;
            currentDiceLaunch.dices.list [7].isSelected = true;
            buttonStartDices.IsEnabled = true;
            buttonStartDices.Visibility = Visibility.Visible;
            buttonEnd.IsEnabled = true;
        }
    }
}
