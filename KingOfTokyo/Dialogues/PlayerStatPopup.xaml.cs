﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KingOfTokyo.Dialogues {
    /// <summary>
    /// Logique d'interaction pour PlayerStatPopup.xaml
    /// </summary>
    public partial class PlayerStatPopup : UserControl {
        public PlayerStatPopup() {

            InitializeComponent();

            cardImage1.Source = null;
            cardImage2.Source = null;
            cardImage3.Source = null;

            HidePopup();
        }

        private void label_MouseEnter(object sender, MouseEventArgs e) {

        }

        private void label_MouseLeave(object sender, MouseEventArgs e) {

        }

        private void ShowPopup() {
            BackgroundImage.Visibility = Visibility.Visible;
            EnergyImage.Visibility = Visibility.Visible;
            labelEnergy.Visibility = Visibility.Visible;
            labelHealth.Visibility = Visibility.Visible;
            cardImage1.Visibility = Visibility.Visible;
            cardImage2.Visibility = Visibility.Visible;
            cardImage3.Visibility = Visibility.Visible;

            Canvas parent = ((Canvas)this.Parent);
            parent.Children.Remove(this);
            parent.Children.Add(this);
        }

        private void HidePopup() {
            BackgroundImage.Visibility = Visibility.Hidden;
            EnergyImage.Visibility = Visibility.Hidden;
            labelEnergy.Visibility = Visibility.Hidden;
            labelHealth.Visibility = Visibility.Hidden;
            cardImage1.Visibility = Visibility.Hidden;
            cardImage2.Visibility = Visibility.Hidden;
            cardImage3.Visibility = Visibility.Hidden;
        }

        private void Rectangle_MouseEnter(object sender, MouseEventArgs e) {
            ShowPopup();
            Console.WriteLine("Show");



        }

        private void Rectangle_MouseLeave(object sender, MouseEventArgs e) {
            HidePopup();
            Console.WriteLine("Hide");
        }
    }
}
