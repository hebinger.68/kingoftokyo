﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KingOfTokyo.Game;
using KingOfTokyo.Network;
using KingOfTokyo.Network.Messages;
using System.Threading;
using KingOfTokyo.Game.Cards;

namespace KingOfTokyo.Server {
    class Server : IReceiver {
        TCPServer server;

        List<Player> players = new List<Player>();
        List<Card> deck;
        Card [] shop = new Card [3];
        CardDeck cardDeck;
        List<string> monster = new List<string>();
        Player kingOfTokyoCity;
        Player kingOfTokyoBay;
        Player lastAttacker;
        PlayerLaunchDices lastLaunch;
        int actual;
        int next;


        private Random random = new Random();

        public Server() {
            this.server = new TCPServer(this, Constants.portServer);
            this.server.StartObjectReceiver();
            InitializeDeck();
            InitializeShop();
            InitializeMonster();
        }

        private void InitializeDeck() {
            cardDeck = new CardDeck();
            deck = cardDeck.GetCards();
        }

        private void InitializeShop() {
            for (int i = 0; i < 3; i++) {
                int value = random.Next(0, deck.Count);
                shop [i] = deck [value];
                deck.RemoveAt(value);
            }
        }

        private void InitializeMonster() {
            monster.Add("Alienoid");
            monster.Add("CyberKitty");
            monster.Add("GigaZaur");
            monster.Add("MecaDragon");
            monster.Add("SpacePinguin");
            monster.Add("TheKing");
        }

        public object ReceiveMessage(IMessage message) {

            if (message.GetType() == typeof(PlayerJoin)) {
                PlayerJoinMessage((PlayerJoin)message);
            } else if (message.GetType() == typeof(StartGame)) {
                UpdateShopMessage();
                StartGameMessage((StartGame)message);
                PlayerStartTurnMessage();
                PlayerLaunchDicesMessage();
            } else if (message.GetType() == typeof(PlayerLaunchDices)) {
                PlayerLaunchDicesMessage((PlayerLaunchDices)message);
            } else if (message.GetType() == typeof(PlayerBoughtCard)) {
                PlayerBuyCard((PlayerBoughtCard)message);
            } else if (message.GetType() == typeof(PlayerEndTurn)) {
                PlayerEndTurnMessage((PlayerEndTurn)message);
            } else if (message.GetType() == typeof(PlayerWantToLeave)) {
                PlayerWantToLeaveMessage((PlayerWantToLeave)message);
            } else if (message.GetType() == typeof(PlayerActivatePowerCard)) {
                PlayerActivatePowerCardMessage((PlayerActivatePowerCard)message);
            } else if (message.GetType() == typeof(PlayerShuffleShop)) {
                PlayerShuffleShopMessage((PlayerShuffleShop)message);
            } else if (message.GetType() == typeof(TestServerOnline)) {
                TestServerOnlineMessage((TestServerOnline)message);
            }

            return 0;
        }

        private void TestServerOnlineMessage(TestServerOnline message) {
            message.online = true;
            if (players.Count() > 5) {
                message.full = true;
            }
            Network.TCPClient.SendObject(message, message.ip, message.port);
        }

        private void PlayerShuffleShopMessage(PlayerShuffleShop message) {
            Player player = players.Where(p => p == message.player).First();
            if (player.HasEnoughEnergy(2)) {
                player.ChangeEnergyPoint(-2);
                for (int i = 0; i < 3; i++) {
                    deck.Add(shop [i]);
                }
                InitializeShop();
                UpdateShopMessage();
                UpdatePlayers();
            }
        }

        private void PlayerActivatePowerCardMessage(PlayerActivatePowerCard message) {
            Player playerActivatePowerCard = players.Where(p => p == message.player).First();
            foreach (CardPower playerCards in playerActivatePowerCard.cards) {
                if (playerCards.id == message.cardId) {
                    if (playerCards.name.Equals("Ailes")) {
                        if (playerActivatePowerCard.HasEnoughEnergy(2) && !playerCards.isActivate) {
                            playerActivatePowerCard.ChangeEnergyPoint(-2);
                            playerCards.isActivate = true;
                        }
                    }
                    if (playerCards.name.Equals("GuerisonRapide")) {
                        if (playerActivatePowerCard.HasEnoughEnergy(2)) {
                            playerActivatePowerCard.heal(1);
                            playerActivatePowerCard.ChangeEnergyPoint(-2);
                        }
                    }
                    if (playerCards.name.Equals("BoissonEnergisante")) {
                        if (playerActivatePowerCard.HasEnoughEnergy(1) && !lastLaunch.hasFinished && lastLaunch.player == message.player) {
                            playerActivatePowerCard.ChangeEnergyPoint(-1);
                            lastLaunch.launchCount--;

                            foreach (Dices.Dice dice in lastLaunch.dices.list) {
                                dice.isPlayable = dice.isUsed;
                            }
                            foreach (Player player in players) {
                                Network.TCPClient.SendObject(lastLaunch, player.ip, player.port);
                            }
                        }
                    }
                    UpdatePlayers();
                }
            }
        }

        private void PlayerWantToLeaveMessage(PlayerWantToLeave message) {
            Player playerWantToLeave = players.Where(player => player == message.player).First();
            if (playerWantToLeave.canLeave) {
                if (kingOfTokyoCity.HasCard("Fouisseur")) {
                    lastAttacker.hurt(1);
                }
                if (players.Where(p => p.isAlive == true).Count() < 5) {
                    kingOfTokyoCity = lastAttacker;
                    lastAttacker.ChangeVictoryPoint(1);
                } else if (kingOfTokyoBay is null) {
                    kingOfTokyoCity = lastAttacker;
                    lastAttacker.ChangeVictoryPoint(1);
                } else {
                    kingOfTokyoCity = kingOfTokyoBay;
                    kingOfTokyoBay = lastAttacker;
                    lastAttacker.ChangeVictoryPoint(1);
                }
                playerWantToLeave.canLeave = false;
                UpdatePlayers();
            }
        }

        private void PlayerEndTurnMessage(PlayerEndTurn message) {
            int playersAlive = players.Where(p => p.isAlive).Count();
            if (playersAlive == 1) {
                EndGameMessage(players.Where(p => p.isAlive).First());
            } else if (playersAlive == 0) {
                EndGameMessage();
            } else if (players [actual].victoryPoint >= 20) {
                EndGameMessage(players [actual]);
            } else {
                if (players [actual].HasCard("EnergieSolaire")) {
                    ApplyPowerCard("EnergieSolaire");
                }
                if (players [actual].HasCard("ReserveDenergie")) {
                    ApplyPowerCard("ReserveDenergie");
                }
                if (players [actual].HasCard("TropMignon")) {
                    ApplyPowerCard("TropMignon");
                }
                foreach (Player player in players) {
                    foreach (CardPower card in player.cards) {
                        if (card.name.Equals("Ailes")) {
                            card.isActivate = false;
                        }
                    }
                }
                while (!players [next].isAlive) {
                    if (next < players.Count() - 1) {
                        next++;
                        if (!players [next].isAlive) {
                            next++;
                        }
                    } else {
                        next = 0;
                    }
                }
                actual = next;
                PlayerStartTurnMessage();
                PlayerLaunchDicesMessage();
            }
        }

        private void EndGameMessage(Player winner = null) {
            EndGame message = new EndGame();
            message.player = winner;
            foreach (Player player in players) {
                Network.TCPClient.SendObject(message, player.ip, player.port);
            }
        }

        private void PlayerBuyCard(PlayerBoughtCard message) {
            for (int i = 0; i < shop.Count(); i++) {
                if (shop [i].id == message.cardId) {
                    for (int j = 0; j < players.Count(); j++) {
                        if (players [j].id == message.player.id) {
                            if (shop [i].CanBuy(players [j])) {
                                if (players [j].HasCard("Mediatique")) {
                                    players [j].ChangeVictoryPoint(1);
                                }
                                if (players [j].HasCard("OrigineExtraTerrestre")) {
                                    players [j].ChangeEnergyPoint(1);
                                }
                                if (shop [i].GetType() == typeof(CardAction)) {
                                    players [j].energyPoint -= shop [i].energyCost;
                                    ApplyActionCard((CardAction)shop [i]);
                                    int value = random.Next(0, deck.Count);
                                    deck.Add(shop [i]);
                                    shop [i] = deck [value];
                                    deck.RemoveAt(value);
                                } else if (shop [i].GetType() == typeof(CardPower)) {
                                    if (players [j].cards.Count() < 3) {
                                        players [j].energyPoint -= shop [i].energyCost;
                                        players [j].cards.Add((CardPower)shop [i]);
                                        ApplyPowerCard(shop [i].name);
                                        int value = random.Next(0, deck.Count);
                                        shop [i] = deck [value];
                                        deck.RemoveAt(value);
                                    }
                                } else {
                                    throw new NotImplementedException("card not implemented");
                                }
                            }
                        }
                    }
                }
            }

            UpdateShopMessage();
            UpdatePlayers();
        }

        private void UpdatePlayers() {
            CheckIfPlayerIsDead();
            foreach (Player player in players) {
                if (player == kingOfTokyoCity) {
                    player.position = PlayerPosition.cityKing;
                } else if (player == kingOfTokyoBay) {
                    player.position = PlayerPosition.bayKing;
                } else {
                    player.position = PlayerPosition.outside;
                }
            }
            UpdatePlayer message = new UpdatePlayer() { players = players };
            foreach (Player player in players) {
                Network.TCPClient.SendObject(message, player.ip, player.port);
            }
        }



        private void PlayerLaunchDicesMessage(PlayerLaunchDices message) {
            if (!message.hasFinished) {
                message.launchCount++;
                PlayerLaunchDices(message);
            }

            if (kingOfTokyoCity is null && message.dices.list.Where(d => d.face == DiceFaces.hurt).Any()) {
                PlayerApplyDices(message);
                kingOfTokyoCity = players.Where(p => p == message.player).First();
                players.Where(p => p == message.player).First().ChangeVictoryPoint(1);
                foreach (Dices.Dice dice in message.dices.list) {
                    dice.isPlayable = false;
                }
                UpdatePlayers();
            } else {
                if (!message.hasFinished) {
                    foreach (Dices.Dice dice in message.dices.list) {
                        dice.isPlayable = dice.isUsed;
                    }
                } else {
                    foreach (Dices.Dice dice in message.dices.list) {
                        dice.isPlayable = false;
                    }
                    PlayerApplyDices(message);
                    UpdatePlayers();
                }
            }
            int maxLaunch = 3;
            if (players [actual].HasCard("CerveauDemesure")) {
                maxLaunch++;
            }
            if (message.launchCount == maxLaunch) {
                foreach (Dices.Dice dice in message.dices.list) {
                    dice.isPlayable = false;
                }
                if (players[actual].HasCard("NinjaUrbain"))
                {
                    message.launchCount--;
                    foreach (Dices.Dice dice in message.dices.list)
                    {
                        dice.isPlayable = dice.face == DiceFaces.three;
                    }
                }
            }

            lastLaunch = message;

            foreach (Player player in players) {
                Network.TCPClient.SendObject(message, player.ip, player.port);
            }


            if (message.hasFinished) {
                PlayerStartBuyCard startBuyMessage = new PlayerStartBuyCard() { player = message.player };
                foreach (Player player in players) {
                    Network.TCPClient.SendObject(startBuyMessage, player.ip, player.port);
                }
            }

        }

        private void PlayerApplyDices(PlayerLaunchDices message) {
            message.hasFinished = true;
                lastAttacker = players [actual];

            int health = message.dices.list.Count(d => d.face == DiceFaces.heal);
            int energy = message.dices.list.Count(d => d.face == DiceFaces.energy);
            int hurt = message.dices.list.Count(d => d.face == DiceFaces.hurt);
            int number1 = message.dices.list.Count(d => d.face == DiceFaces.one);
            int number2 = message.dices.list.Count(d => d.face == DiceFaces.two);
            int number3 = message.dices.list.Count(d => d.face == DiceFaces.three);
            if (energy >= 1) {
                if (players [actual].HasCard("LAmiDesEnfants")) {
                    energy++;
                }
            }
            if (health >= 1) {
                if (players [actual].HasCard("Regeneration")) {
                    health++;
                }
            }
            if (health == 1 && energy == 1 && hurt == 1 && number1 == 1 && number2 == 1 && number3 == 1) {
                if (players [actual].HasCard("DestructionTotale")) {
                    players [actual].ChangeVictoryPoint(9);
                }
            }
            if (number1 >= 1 && number2 >= 1 && number3 >= 1) {
                if (players [actual].HasCard("Detritivore")) {
                    players [actual].ChangeVictoryPoint(2);
                }
            }
            if (kingOfTokyoCity is null) {
                hurt = 0;
            }
            if (!players [actual].IsInCity) {
                players [actual].heal(health);
            }
            players [actual].energyPoint += energy;
            if (number1 >= 3) {
                if (players [actual].HasCard("Gourmet")) {
                    players [actual].ChangeVictoryPoint(2);
                }
                players [actual].ChangeVictoryPoint(number1 - 2);
            }
            if (number2 >= 3) {
                if (players [actual].HasCard("DardsEmpoisonnes")) {
                    hurt += 2;
                }
                players [actual].ChangeVictoryPoint(number2 - 1);
            }
            if (number3 >= 3) {
                players [actual].ChangeVictoryPoint(number3);
            }
            if (players [actual].HasCard("AttaqueAcide") && kingOfTokyoCity != null) {
                hurt++;
            }
            if (hurt != 0) {
                if (players [actual].HasCard("QueueAPiquants")) {
                    hurt++;
                }
                if (players [actual].HasCard("MonstreAlpha")) {
                    players [actual].ChangeVictoryPoint(1);
                }
                foreach (Player player in players.Where(player => player != players [actual])) {
                    if (players [actual].HasCard("SouffleNova")) {
                        player.hurt(hurt);
                    } else if ((players [actual].IsInCity) && (player.position == PlayerPosition.outside)) {
                        if (players [actual].HasCard("Fouisseur")) {
                            hurt++;
                        }
                        if (players [actual].HasCard("Urbavore")) {
                            hurt++;
                        }
                        player.hurt(hurt);
                    } else if ((player.IsInCity) && (players [actual].position == PlayerPosition.outside)) {
                        player.hurt(hurt);
                        if (player.position == PlayerPosition.cityKing) {
                            player.canLeave = true;
                        }
                        if (kingOfTokyoBay == null && players.Where(p => p.isAlive == true).Count() > 4) {
                            kingOfTokyoBay = players [actual];
                        }
                    }
                }
            }
        }

        private void PlayerLaunchDices(PlayerLaunchDices message) {


            foreach (Dices.Dice dice in message.dices.list) {
                if (dice.isSelected && dice.isUsed) {
                    Array faces = Enum.GetValues(typeof(DiceFaces));
                    DiceFaces randomFace = (DiceFaces)faces.GetValue(random.Next(1, faces.Length));

                    dice.face = randomFace;
                }
            }
        }

        private void UpdateShopMessage() {
            UpdateShop message = new UpdateShop() { shop = this.shop };
            foreach (Player player in players) {
                Network.TCPClient.SendObject(message, player.ip, player.port);
            }
        }

        private void InitializePlayer(Player player) {
            player.healthPoint = 10;
            player.SetVictoryPoint(0);
            player.energyPoint = 0;
            player.position = PlayerPosition.outside;
            player.isAlive = true;
            int value = random.Next(0, monster.Count);
            player.monsterName = monster [value];
            monster.RemoveAt(value);
        }

        private void PlayerStartTurnMessage() {
            if (next < players.Count() - 1) {
                next++;
                if (!players [next].isAlive) {
                    next++;
                }
            } else {
                next = 0;
            }
            foreach (Player player in players.Where(p => p.isAlive)) {
                player.canLeave = false;
            }
            PlayerStartTurn start = new PlayerStartTurn() { player = players [actual] };
            foreach (Player player in players) {
                Network.TCPClient.SendObject(start, player.ip, player.port);
            }
            if (players [actual].IsInCity) {
                players [actual].ChangeVictoryPoint(2);
                if (players [actual].HasCard("Urbavore")) {
                    players [actual].ChangeVictoryPoint(1);
                }
                UpdatePlayers();
            }

        }

        private void StartGameMessage(StartGame message) {
            foreach (Player player in players) {
                Network.TCPClient.SendObject(message, player.ip, player.port);
            }
            actual = 0;
            next = 0;
        }

        private void PlayerLaunchDicesMessage() {
            int numberBonusDices = players [actual].cards.Where(c => c.name.Equals("UneTeteDePlus")).Count();
            Dices dices = new Dices(numberBonusDices);
            PlayerLaunchDices diceMessage = new PlayerLaunchDices() { player = players [actual], dices = dices, launchCount = 0, hasFinished = false };

            foreach (Player player in players) {
                Network.TCPClient.SendObject(diceMessage, player.ip, player.port);
            }
        }

        public void PlayerJoinMessage(PlayerJoin message) {

            Player playerJoining = message.player;

            InitializePlayer(playerJoining);

            Console.WriteLine("Player " + playerJoining.ip + " " + playerJoining.name + " " + playerJoining.id);

            foreach (Player player in players) {
                PlayerJoin messageForJoining = new PlayerJoin() { player = player };
                Network.TCPClient.SendObject(messageForJoining, playerJoining.ip, playerJoining.port);
            }

            players.Add(playerJoining);

            foreach (Player player in players) {
                Network.TCPClient.SendObject(message, player.ip, player.port);
            }
        }

        private void ApplyActionCard(CardAction card) {
            switch (card.name) {
                case "AvionsDeChasse":
                    players [actual].ChangeVictoryPoint(5);
                    players [actual].hurt(4);
                    break;
                case "BombardementAerien":
                    foreach (Player player in players.Where(p => p.isAlive)) {
                        player.hurt(3);
                    }
                    break;
                case "CentraleNucleaire":
                    players [actual].ChangeVictoryPoint(2);
                    players [actual].heal(3);
                    break;
                case "GardeNationale":
                    players [actual].ChangeVictoryPoint(2);
                    players [actual].hurt(2);
                    break;
                case "GratteCiel":
                    players [actual].ChangeVictoryPoint(4);
                    break;
                case "Immeuble":
                    players [actual].ChangeVictoryPoint(3);
                    break;
                case "LaMortVientDuCiel":
                    players [actual].ChangeVictoryPoint(2);
                    if (!players [actual].IsInCity) {
                        players [actual].ChangeVictoryPoint(1);
                    }
                    kingOfTokyoCity = players [actual];
                    players [actual].canLeave = false;
                    kingOfTokyoCity.canLeave = false;
                    break;
                case "LanceFlammes":
                    foreach (Player player in players.Where(p => p != players [actual] && p.isAlive)) {
                        player.hurt(2);
                    }
                    break;
                case "LaPetiteBoutiqueDuCoin":
                    players [actual].ChangeVictoryPoint(1);
                    break;
                case "OrdreDevacuation":
                    foreach (Player other in players.Where(p => p != players [actual] && p.isAlive)) {
                        other.ChangeVictoryPoint(-5);
                    }
                    break;
                case "RaffinerieDeGaz":
                    players [actual].ChangeVictoryPoint(2);
                    foreach (Player other in players.Where(p => p != players [actual] && p.isAlive)) {
                        other.hurt(3);
                    }
                    break;
                case "Rage":
                    next = actual;
                    break;
                case "Recharge":
                    players [actual].energyPoint += 9;
                    break;
                case "Soin":
                    players [actual].heal(2);
                    break;
                case "Tank":
                    players [actual].ChangeVictoryPoint(4);
                    players [actual].hurt(3);
                    break;
                case "TempeteMagnetique":
                    players [actual].ChangeVictoryPoint(2);
                    foreach (Player other in players.Where(p => p != players [actual] && p.isAlive)) {
                        other.energyPoint -= other.energyPoint / 2;
                    }
                    break;
                case "Tramway":
                    players [actual].ChangeVictoryPoint(2);
                    break;



                default:
                    throw new Exception("Card not found : " + card.name);
            }
        }

        private void ApplyPowerCard(string name) {
            switch (name) {
                case "EncorePlusGrand":
                    players [actual].maxHealth = 12;
                    players [actual].heal(2);
                    break;
                case "EnergieSolaire":
                    if (players [actual].energyPoint == 0) {
                        players [actual].energyPoint++;
                    }
                    break;
                case "ReserveDenergie":
                    players [actual].ChangeVictoryPoint(players [actual].energyPoint / 6);
                    break;
                case "TropMignon":
                    int min = players [actual].victoryPoint;
                    foreach (Player player in players.Where(p => p.isAlive)) {
                        if (min > player.victoryPoint) {
                            return;
                        }
                    }
                    players [actual].ChangeVictoryPoint(1);
                    break;
                default:
                    break;
            }
        }

        private void CheckIfPlayerIsDead() {
            foreach (Player player in players) {
                if (player.healthPoint == 0) {
                    if (player.HasCard("IlRestaitUnOeuf")) {
                        player.SetVictoryPoint(0);
                        player.heal(10);
                        foreach (CardPower card in player.cards) {
                            deck.Add(card);
                        }
                        player.cards.Clear();
                    } else {
                        player.isAlive = false;
                        foreach (CardPower card in player.cards) {
                            deck.Add(card);
                        }
                        player.cards.Clear();
                        foreach (Player p in players) {
                            if (p.HasCard("Necrophage") && p.id != player.id) {
                                p.ChangeVictoryPoint(3);
                            }
                        }
                    }
                    if (lastAttacker == player)
                    {
                        if (player == kingOfTokyoCity)
                        {
                            if (kingOfTokyoBay != null)
                            {
                                kingOfTokyoCity = kingOfTokyoBay;
                                kingOfTokyoBay = null;
                            }
                            else
                            {
                                kingOfTokyoCity = null;
                            }
                        }
                        else
                        {
                            kingOfTokyoBay = null;
                        }
                    }
                    else if (player.IsInCity)
                    {
                        if (player.position == PlayerPosition.bayKing)
                        {
                            if (lastAttacker != kingOfTokyoCity)
                            {
                                kingOfTokyoBay = lastAttacker;
                                lastAttacker.ChangeVictoryPoint(1);
                            }
                            else
                            {
                                kingOfTokyoBay = null;
                            }
                        } 
                        else
                        {
                            if (lastAttacker != kingOfTokyoBay)
                            {
                                lastAttacker.ChangeVictoryPoint(1);
                            }
                            else
                            {
                                kingOfTokyoBay = null;
                            }
                            kingOfTokyoCity = lastAttacker;
                        }
                        player.canLeave = false;
                    }
                }
            }
        }
    }



}
